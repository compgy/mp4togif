import React, { useState, useEffect } from 'react';
import './App.css';

import { createFFmpeg, fetchFile } from '@ffmpeg/ffmpeg';
const ffmpeg = createFFmpeg({ log: true });

function App() {
  const [ready, setReady] = useState(false);
  const [video, setVideo] = useState();
  const [gif, setGif] = useState();
  const [isRunning, setRunning] = useState();

  let url;
  const load = async () => {
    await ffmpeg.load();
    setReady(true);
  };

  useEffect(() => {
    load();
  }, []);

  const convertToGif = async () => {
    setRunning(true);
    // Write the file to memory
    ffmpeg.FS('writeFile', 'test.mp4', await fetchFile(video));

    // Run the FFMpeg command
    await ffmpeg.run('-i', 'test.mp4', '-f', 'gif', 'out.gif');

    // Read the result
    const data = ffmpeg.FS('readFile', 'out.gif');

    // Create a URL
    url = URL.createObjectURL(new Blob([data.buffer], { type: 'image/gif' }));
    console.log(url);
    setGif(url);
    setRunning(false);
    setDone(true);
  };

  if (ready) {
    if (!isRunning) {
      return (
        <div className="App">
          {video && (
            <video
              controls
              width="500"
              src={URL.createObjectURL(video)}
            ></video>
          )}

          <input
            type="file"
            onChange={(e) => setVideo(e.target.files?.item(0))}
          />

          <h3>Result</h3>

          <button onClick={convertToGif}>Convert</button>

          {gif && <img src={gif} width="500" />}
          <input type="button" value="Download" />
        </div>
      );
    } else {
      return (
        <div className="App">
          {video && (
            <video
              controls
              width="500"
              src={URL.createObjectURL(video)}
            ></video>
          )}

          <h3>Result</h3>
          <svg
            class="spinner"
            width="65px"
            height="65px"
            viewBox="0 0 66 66"
            xmlns="http://www.w3.org/2000/svg"
          >
            <circle
              class="path"
              fill="none"
              stroke-width="6"
              stroke-linecap="round"
              cx="33"
              cy="33"
              r="30"
            ></circle>
          </svg>
          {gif && <img src={gif} width="500" />}
        </div>
      );
    }
  } else {
    return (
      <svg
        class="spinner"
        width="65px"
        height="65px"
        viewBox="0 0 66 66"
        xmlns="http://www.w3.org/2000/svg"
      >
        <circle
          class="path"
          fill="none"
          stroke-width="6"
          stroke-linecap="round"
          cx="33"
          cy="33"
          r="30"
        ></circle>
      </svg>
    );
  }
}

export default App;
